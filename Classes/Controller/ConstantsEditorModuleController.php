<?php

namespace DKM\ConstantsEditor\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use DKM\CacheControl\Utility\FrontendCache;
use DKM\ConstantsEditor\Helpers\MiscHelper;
use DKM\ConstantsEditor\Template\ModuleTemplate;
use http\Exception\BadMethodCallException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Form\FormResultCompiler;
use TYPO3\CMS\Backend\Routing\PreviewUriBuilder;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Template\Components\Buttons\LinkButton;
use TYPO3\CMS\Backend\Template\Components\Buttons\SplitButton;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Html\HtmlParser;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateModuleController;

/**
 * Module: TypoScript Tools
 * @internal This is a specific Backend Controller implementation and is not considered part of the Public TYPO3 API.
 */
class ConstantsEditorModuleController extends TypoScriptTemplateModuleController
{

    protected $moduleMethodName;

    private array $originalPermissions;

    private string $moduleTitle;

    /**
     * Module request redirect to mainAction
     * @param $name
     * @param $arguments
     * @return ResponseInterface
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        /** @var ServerRequestInterface $request */
        $request = $arguments[0];
        $this->setModuleMethodName($request->getAttribute('route')->getOption('moduleName'));
        return $this->mainAction($request);
    }

    /**
     * Overriden to:
     * - force page id to root page id
     * - force constants category belonging to BE module
     * - add Tabs JS
     * - override permissions to be able to save
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function mainAction(ServerRequestInterface $request): ResponseInterface
    {
        $this->moduleTemplate = GeneralUtility::makeInstance(ModuleTemplate::class);

        $config = $this->getModuleConfiguration();

        if ($request->getQueryParams()['usePageIdFromRequest'] ?? false) {
            $this->id = (int)($request->getParsedBody()['id'] ?? $request->getQueryParams()['id'] ?? 0);
        } else {

            if ($config['constantsEditor']['pidFromUserTS'] ?? false) {
                try {
                    $this->id = ArrayUtility::getValueByPath(GeneralUtility::removeDotsFromTS($GLOBALS['BE_USER']->userTS), $config['constantsEditor']['pidFromUserTS'] ?? [], '.') ?? 0;
                } catch (MissingArrayPathException $exception) {
                }
            }
            if (!$this->id) {
                if(!$this->id = MiscHelper::getPageId()) {
                    throw new Exception('No page id could be found. Maybe you are admin with no webmount?');
                };
            }
        }


        $this->moduleTitle = $config['constantsEditor']['moduleTitle'];
        $constantCategory = $config['constantsEditor']['constantCategory'];
        $request = $request->withParsedBody(array_merge((array)$request->getParsedBody(), ['id' => $this->id,
            'SET' => ['constant_editor_cat' => $constantCategory]]));

        //TypoScriptTemplateConstantEditorModuleFunctionController reads the id from $_POST var
//        $this->perms_clause = $this->getBackendUser()->getPagePermsClause(Permission::PAGE_SHOW);

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook']['constantsEditor'] = function ($params, $that) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['\DKM\ConstantsEditor\Controller\ConstantsEditorModuleController']['getButtonsHook'] ?? [] as $funcRef) {
                $params['id'] = $this->id;
                $params = GeneralUtility::callUserFunction($funcRef, $params, $this);
                break;
            }
            return $params['buttons'];
        };



        if (ExtensionManagementUtility::isLoaded('cachecontrol')) {
            FrontendCache::flushSiteFromPageUid($this->id);
        }
        $this->modifyPermissions();
        $formResultCompiler = GeneralUtility::makeInstance(FormResultCompiler::class);
        $formResultCompiler->printNeededJSFunctions();
        $result = parent::mainAction($request);
        $this->restorePermissions();
        //@todo inject page renderer
        $this->moduleTemplate->getPageRenderer()->loadRequireJsModule('TYPO3/CMS/Backend/Tabs');
        return $result;
    }


    /**
     * Overriden: To remove function menu
     *
     * Initializes the internal MOD_MENU array setting and unsetting items based on various conditions. It also merges in external menu items from the global array TBE_MODULES_EXT (see mergeExternalItems())
     * Then MOD_SETTINGS array is cleaned up (see \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()) so it contains only valid values. It's also updated with any SET[] values submitted.
     * Also loads the modTSconfig internal variable.
     *
     * @param array $changedSettings can be anything
     * @see mainAction()
     * @see \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()
     * @see mergeExternalItems()
     */
    protected function menuConfig($changedSettings)
    {
        parent::menuConfig($changedSettings);
        $this->MOD_MENU['function'] = [];
    }

    /**
     * Overriden: To ensure using constantseditor Main.html template and set moduleTitle
     * Returns a new standalone view, shorthand function
     *
     * @param string $extensionName
     * @param string $templateName
     * @return StandaloneView
     */
    protected function getFluidTemplateObject($extensionName, $templateName = 'Main')
    {
        $extensionName = 'constantseditor';
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->getRenderingContext()->getTemplatePaths()->fillDefaultsByPackageName($extensionName);
        $view->getRenderingContext()->setControllerAction($templateName);
        $view->getRequest()->setControllerExtensionName($extensionName);

        $urlParameters = [
            'id' => $this->id,
            'template' => 'all'
        ];
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $aHref = (string)$uriBuilder->buildUriFromRoute($this->getModuleMethodName(), $urlParameters);

        $view->assign('actionNameConstantEditor', $aHref);
        $view->assign('moduleTitle', $this->moduleTitle);
        return $view;
    }

    /**
     * Open op permissions to be able to save updates
     */
    private function modifyPermissions()
    {
        // Add sys_template to the list of allowed tables.
        $this->originalPermissions['ModifyTables'] = $GLOBALS['BE_USER']->groupData['tables_modify'];
        $GLOBALS['BE_USER']->groupData['tables_modify'] .= ',sys_template';
        $this->originalPermissions['ExcludeFields'] = $GLOBALS['BE_USER']->groupData['non_exclude_fields'];
        $GLOBALS['BE_USER']->groupData['non_exclude_fields'] .= ',sys_template:skin_selector';
        $this->originalPermissions['SysTemplateAdminOnly'] = $GLOBALS['TCA']['sys_template']['ctrl']['adminOnly'];
        $GLOBALS['TCA']['sys_template']['ctrl']['adminOnly'] = 0;
    }

    /**
     * Restore original permissions
     */
    private function restorePermissions()
    {
        // Restore original permissions
        $GLOBALS['BE_USER']->groupData['tables_modify'] = $this->originalPermissions['ModifyTables'];
        $GLOBALS['TCA']['sys_template']['ctrl']['adminOnly'] = $this->originalPermissions['SysTemplateAdminOnly'];
        $GLOBALS['BE_USER']->groupData['non_exclude_fields'] = $this->originalPermissions['ExcludeFields'];
    }

    /**
     * Overridden: to always show the constant editor module content
     * Return the content of the 'main' function inside the "Function menu module" if present
     *
     * @return string
     */
    protected function getExtObjContent()
    {
        $htmlParser = GeneralUtility::makeInstance(HtmlParser::class);
        $savedContent = parent::getExtObjContent();
        $className = 'tstemplate-constanteditor';
        $domPart = '';
        foreach ($htmlParser->splitIntoBlock('div', $savedContent) as $domPart) {
            if (strpos($domPart, $className) !== false) {
                break;
            }
        }
        return $domPart;
    }


    /**
     * GETTERS AND SETTERS of properties
     */

    /**
     * @param $name
     */
    public function setModuleMethodName($name)
    {
        $this->moduleMethodName = $name;
        $this->moduleName = $name;
    }

    /**
     * @return mixed
     */
    public function getModuleMethodName()
    {
        return $this->moduleMethodName;
    }

    /**
     * @return bool|array
     */
    public function getModuleConfiguration()
    {
        if (isset($GLOBALS['TBE_MODULES']['_configuration'][$this->getModuleMethodName()])) return $GLOBALS['TBE_MODULES']['_configuration'][$this->getModuleMethodName()];
        return false;
    }
}
