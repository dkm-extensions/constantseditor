<?php
/**
 * Created by PhpStorm.
 * User: stigfaerch
 * Date: 02/05/2019
 * Time: 13.14
 */

namespace DKM\ConstantsEditor\Controller;


use DKM\ConstantsEditor\Helpers\MiscHelper;
use DKM\ConstantsEditor\TypoScript\TemplateServiceFactory;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateModuleController;

class TypoScriptTemplateConstantEditorModuleFunctionController extends \TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateConstantEditorModuleFunctionController
{
    /**
     * Overridden:
     * - To use ExtendedTemplateService from EXT:constantseditor instead of ExtendedTemplateService from EXT:tstemplate
     * - To execute makeConstantsAccessArray method from ExtendedTemplateServiceBase
     *
     * Initialize editor
     *
     * Initializes the module.
     * Done in this function because we may need to re-initialize if data is submitted!
     *
     * @param int $pageId
     * @param int $template_uid
     * @return bool
     */
    protected function initialize_editor($pageId, $template_uid = 0)
    {
        if($this->isTemplateModule($this->pObj)) {
            return parent::initialize_editor($pageId, $template_uid);
        } else {
            return $this->initialize_editor_modified($pageId, $template_uid);
        }
    }

    /**
     * Overridden:
     * - To use ExtendedTemplateService from EXT:constantseditor instead of ExtendedTemplateService from EXT:tstemplate
     * - To execute makeConstantsAccessArray method from ExtendedTemplateServiceBase
     *
     * v10 and v11 original methods almost identical
     *
     * Initialize editor
     *
     * Initializes the module.
     * Done in this function because we may need to re-initialize if data is submitted!
     *
     * @param int $pageId
     * @param int $template_uid
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initialize_editor_modified($pageId, $template_uid = 0)
    {

// @note changed
        $this->templateService = GeneralUtility::makeInstance(TemplateServiceFactory::class)->getTemplateService();

        // Get the row of the first VISIBLE template of the page. whereclause like the frontend.
        $this->templateRow = $this->templateService->ext_getFirstTemplate($pageId, $template_uid);
        // IF there was a template...
        if (is_array($this->templateRow)) {
            // Gets the rootLine
            $rootLineUtility = GeneralUtility::makeInstance(RootlineUtility::class, $pageId);
            $rootLine = $rootLineUtility->get();
            // This generates the constants/config + hierarchy info for the template.
            $this->templateService->runThroughTemplates($rootLine, $template_uid);
            // The editable constants are returned in an array.
            $this->constants = $this->templateService->generateConfig_constants();

// @note added - Make the constants access array - er denne custom?
$this->templateService->makeConstantsAccessArray($this->id);

            // The returned constants are sorted in categories, that goes into the $tmpl->categories array
            $this->templateService->ext_categorizeEditableConstants($this->constants);
            // This array will contain key=[expanded constant name], value=line number in template.
            $this->templateService->ext_regObjectPositions((string)$this->templateRow['constants']);
            return true;
        }
        return false;
    }

    /**
     * @return string|void
     */
    public function main()
    {
        if($this->isTemplateModule($this->pObj)) {
            return parent::main();
        } else {
            return $this->main_modified();
        }
    }

    /**
     * Based on v11 method
     * Overridden to:
     * - use ConstantEditor.html from EXT:constantseditor instead of EXT:tstemplate
     * - add constant mod.constantseditor.showConstantName as a fluid variable
     * Main, called from parent object
     *
     * @return string
     */
    public function main_modified()
    {
        $assigns = [];
        // Create extension template
        $this->pObj->createTemplate($this->id);
        // Checking for more than one template an if, set a menu...
        $manyTemplatesMenu = $this->pObj->templateMenu($this->request);
        $template_uid = 0;
        if ($manyTemplatesMenu) {
            $template_uid = $this->pObj->MOD_SETTINGS['templatesOnPage'];
        }

        // initialize
        $existTemplate = $this->initialize_editor($this->id, $template_uid);
        if ($existTemplate) {
            $assigns['templateRecord'] = $this->templateRow;
            $assigns['manyTemplatesMenu'] = $manyTemplatesMenu;

            $saveId = empty($this->templateRow['_ORIG_uid']) ? $this->templateRow['uid'] : $this->templateRow['_ORIG_uid'];
            // Update template ?
            if ($this->request->getParsedBody()['_savedok'] ?? false) {
                $this->templateService->changed = false;
                $this->templateService->ext_procesInput($this->request->getParsedBody(), [], $this->constants, $this->templateRow);
                if ($this->templateService->changed) {
                    // Set the data to be saved
                    $recData = [];
                    $recData['sys_template'][$saveId]['constants'] = implode(LF, $this->templateService->raw);
                    // Create new  tce-object
                    $tce = GeneralUtility::makeInstance(DataHandler::class);
                    $tce->start($recData, []);
                    $tce->process_datamap();
                    // re-read the template ...
                    // re-read the constants as they have changed
                    $this->initialize_editor($this->id, $template_uid);
                }
            }
            // Resetting the menu (start). I wonder if this in any way is a violation of the menu-system. Haven't checked. But need to do it here, because the menu is dependent on the categories available.
            $this->pObj->MOD_MENU['constant_editor_cat'] = $this->templateService->ext_getCategoryLabelArray();
            $this->pObj->MOD_SETTINGS = BackendUtility::getModuleData($this->pObj->MOD_MENU, $this->request->getParsedBody()['SET'] ?? $this->request->getQueryParams()['SET'] ?? [], 'web_ts');
            // Resetting the menu (stop)
            $assigns['title'] = $this->pObj->linkWrapTemplateTitle($this->templateRow['title'], 'constants');
            if (!empty($this->pObj->MOD_MENU['constant_editor_cat'])) {
                $assigns['constantsMenu'] = BackendUtility::getDropdownMenu($this->id, 'SET[constant_editor_cat]', $this->pObj->MOD_SETTINGS['constant_editor_cat'], $this->pObj->MOD_MENU['constant_editor_cat']);
            }
            // Category and constant editor config:
            $category = $this->pObj->MOD_SETTINGS['constant_editor_cat'];

// @note added
            $this->additionalViewAssigns($assigns);

            $assigns['editorFields'] = $this->templateService->ext_printFields($this->constants, $category);

// @note uncommented
//            foreach ($this->templateService->getJavaScriptInstructions() as $instruction) {
//                $this->getPageRenderer()->getJavaScriptRenderer()->addJavaScriptModuleInstruction($instruction);
//            }

// @note inserted v10/v11
            $this->javaScriptInclusion();

            // Rendering of the output via fluid
            $view = GeneralUtility::makeInstance(StandaloneView::class);

// @note changed
            $view->setTemplatePathAndFilename('EXT:constantseditor/Resources/Private/Templates/ConstantEditor.html');

            $view->assignMultiple($assigns);
            $theOutput = $view->render();
        } else {
            $theOutput = $this->pObj->noTemplate(1);
        }
        return $theOutput;
    }

    /**
     * @return void
     */
    protected function javaScriptInclusion() {
        if(MiscHelper::isTYPO3Version(11)) {
            foreach ($this->templateService->getJavaScriptInstructions() as $instruction) {
                $this->getPageRenderer()->getJavaScriptRenderer()->addJavaScriptModuleInstruction($instruction);
            }
        } else {
            foreach ($this->templateService->getInlineJavaScript() as $name => $inlineJavaScript) {
                $this->getPageRenderer()->addJsInlineCode($name, $inlineJavaScript);
            }
        }
    }




    /**
     * Init, called from parent object
     *
     * @param TypoScriptTemplateModuleController $pObj A reference to the parent (calling) object
     * @param ServerRequestInterface $request
     */
    public function init($pObj, ServerRequestInterface $request)
    {
        if($this->isTemplateModule($pObj)) {
            parent::init($pObj, $request);
        } else {
            $this->pObj = $pObj;
            $this->request = $request;
            $this->id = (int)($request->getParsedBody()['id'] ?? $request->getQueryParams()['id'] ?? 0);
        }
    }


    /**
     * @return array
     */
    public function getConstants(): array
    {
        return $this->constants;
    }

    /**
     * @param $object
     * @return bool
     */
    protected function isTemplateModule($object): bool
    {
        return get_class($object) == 'TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateModuleController';
    }

    /**
     * @param $assigns
     * @return void
     */
    protected function additionalViewAssigns(&$assigns) {
        $assigns['showConstantName'] = $this->getConstants()['mod.constantseditor.showConstantName']['value'] ?? '';
        $assigns['dataToggleAttribute'] = MiscHelper::isTYPO3Version(10) ? 'data-toggle=tab' : 'data-bs-toggle=tab';
    }

}