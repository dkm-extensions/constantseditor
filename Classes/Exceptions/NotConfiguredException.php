<?php
/**
 * Created by PhpStorm.
 * User: stigfaerch
 * Date: 07/05/2019
 * Time: 13.05
 */

namespace DKM\ConstantsEditor\Exceptions;


use TYPO3\CMS\Core\Exception;

class NotConfiguredException extends Exception
{

}