<?php
namespace DKM\ConstantsEditor\Helpers;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Created by PhpStorm.
 * User: stigfaerch
 * Date: 01/05/2019
 * Time: 15.47
 */

class MiscHelper
{
    /**
     * @return bool|int
     */
    static public function getPageId() {
        // For non-admin users, get the first webmount with a root pid.
        foreach (self::getBackendUser()->returnWebmounts() as $webMountId)	{
            $calculatedPid = self::findRootPid($webMountId);
            if($calculatedPid) return $calculatedPid;
        }
        return false;
    }

    /**
     * Starts at the specified page ID and walks up the tree to find the nearest root page id.
     * This allows other WEC config modules to work relative to the appropriate root page.
     *
     * @param int $pageID
     * @return int
     */
    static public function findRootPid($pageID) {
        if(!$pageID) return false;
        /** @var ExtendedTemplateService $tsTemplate */
        $tsTemplate = GeneralUtility::makeInstance(ExtendedTemplateService::class);
        $tsTemplate->tt_track = 0;

        // Gets the rootLine
        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageID)->get();
        $tsTemplate->runThroughTemplates($rootLine);
        return $tsTemplate->getRootId();
    }

    /**
     * @return BackendUserAuthentication
     */
    static public function getBackendUser() {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @return void
     * @throws Exception
     */
    static public function throwVersionException(): void
    {
        throw new Exception('Only TYPO3 v10 and v11 is supported from this version.');
    }

    /**
     * @param $version
     * @return bool
     */
    static public function isTYPO3Version($version): bool
    {
        return GeneralUtility::makeInstance(Typo3Version::class)->getMajorVersion() == $version;
    }
}