<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace DKM\ConstantsEditor\TypoScript;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Configuration\Features;
use TYPO3\CMS\Core\TypoScript\ExtendedTemplateService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * TSParser extension class to TemplateService
 * Contains functions for the TS module in TYPO3 backend
 *
 * @internal this is only used for the TYPO3 TypoScript Template module, which should not be used in Extensions
 */
class ExtendedTemplateServiceBase extends ExtendedTemplateService
{
    /**
     * @var array
     */
    protected $modTSconfig = [];
    private $constantsAccessArray = [];
    protected $tabsEnabled;
    protected $tabsCatList;

    /**
     * @note added
     * Returns false if constant should not be accessible
     *
     * @param $name
     * @return bool
     */
    protected function constantAllowed($name): bool
    {
        /**
         * Loop through $accessStrings to find if one is matching, if return true else false
         * @param $testString
         * @param $accessStrings
         * @return bool
         */
        $checkRecursive = function ($testString, $accessStrings) {
            $testComponents = explode('.', $testString);
            foreach ($accessStrings as $accessString) {
                $pass = true;
                foreach (explode('.', $accessString ?? []) as $index => $accessStringComponent) {
                    if (!isset($testComponents[$index]) || $testComponents[$index] !== $accessStringComponent) {
                        $pass = false;
                        break;
                    }
                }
                if ($pass) return true;
            }
            return false;
        };

        if ($this->constantsAccessArray) {
            //enables the constant (if constant is not enabled - skip it)
            if (
                !(
                    (isset($this->constantsAccessArray['enable']['single']) && in_array($name, $this->constantsAccessArray['enable']['single']))
                    ||
                    $checkRecursive($name, $this->constantsAccessArray['enable']['recursive'] ?? [])
                )
            ) {
                return false;
            }
            //disables the constant (if constant is disabled - skip it)
            if (
                (isset($this->constantsAccessArray['disable']['single']) && in_array($name, $this->constantsAccessArray['disable']['single']))
                ||
                $checkRecursive($name, $this->constantsAccessArray['disable']['recursive'] ?? [])
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * @note added
     */
    public function makeConstantsAccessArray($pageId)
    {
        if (GeneralUtility::makeInstance(Features::class)->isFeatureEnabled('LP69869872-constantseditor-access-pagets')) {
            $configuration = BackendUtility::getPagesTSconfig($pageId);
            if (isset($configuration['mod.']['constantseditor.'])) {
                $arr = array_intersect_key($configuration['mod.']['constantseditor.'], ['recursive.' => 1, 'single.' => 1]);
                foreach (['recursive', 'single'] as $scope) {
                    $arr["{$scope}."] = array_filter($arr["{$scope}."] ?? [],
                        function ($v, $k) {
                            return !str_ends_with($k, '..');
                        }, ARRAY_FILTER_USE_BOTH);
                    $arrFlatTmp = ArrayUtility::flatten($arr["{$scope}."]);
                    $this->constantsAccessArray['enable'][$scope] = array_keys(array_filter($arrFlatTmp, function ($v, $k) {
                        return (bool)$v;
                    }, ARRAY_FILTER_USE_BOTH));
                    if (!$this->constantsAccessArray['enable'][$scope]) unset ($this->constantsAccessArray['enable'][$scope]);
                    $this->constantsAccessArray['disable'][$scope] = array_keys(array_filter($arrFlatTmp, function ($v, $k) {
                        return !(bool)$v;
                    }, ARRAY_FILTER_USE_BOTH));
                    if (!($this->constantsAccessArray['disable'][$scope])) unset($this->constantsAccessArray['disable'][$scope]);
                }
            }
        } else {
            if (isset($this->setup['constants']['mod.']['constantseditor.'])) {
                $arr = array_intersect_key($this->setup['constants']['mod.']['constantseditor.'], ['recursive.' => 1,
                    'single.' => 1]);
                foreach (['recursive',
                             'single'] as $scope) {
                    $arr["{$scope}."] = array_filter($arr["{$scope}."] ?? [],
                        function ($v, $k) {
                            return !str_ends_with($k, '..');
                        }, ARRAY_FILTER_USE_BOTH);
                    $arrFlatTmp = ArrayUtility::flatten($arr["{$scope}."]);
                    $this->constantsAccessArray['enable'][$scope] = array_keys(array_filter($arrFlatTmp, function ($v, $k) {
                        return (bool)$v;
                    }, ARRAY_FILTER_USE_BOTH));
                    if (!$this->constantsAccessArray['enable'][$scope]) unset ($this->constantsAccessArray['enable'][$scope]);
                    $this->constantsAccessArray['disable'][$scope] = array_keys(array_filter($arrFlatTmp, function ($v, $k) {
                        return !(bool)$v;
                    }, ARRAY_FILTER_USE_BOTH));
                    if (!($this->constantsAccessArray['disable'][$scope])) unset($this->constantsAccessArray['disable'][$scope]);
                }
            }
        }
    }
}