<?php

declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace DKM\ConstantsEditor\TypoScript\Parser;

use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Parser for TypoScript constant configuration lines and values
 * like "# cat=content/cText/1; type=; label= Bodytext font: This is the font face used for text!"
 * for display of the constant editor and extension settings configuration
 *
 * Basic TypoScript parsing is delegated to the TypoScriptParser which returns with comments intact.
 * These comments are then parsed by this class to prepare/set display related options. The Constant
 * Editor renders fields itself whereas the extension settings are rendered by fluid.
 */
class ConstantConfigurationParser extends \TYPO3\CMS\Core\TypoScript\Parser\ConstantConfigurationParser
{
    /**
     * @note added
     * @var array
     */
    protected array $subCategorySorting = [];

    /**
     * Overridden to:
     * - add inline comment to parse (#inline)
     * - add subcategorysorting comment
     *
     * This function compares the flattened constants (default and all).
     * Returns an array with the constants from the whole template which may be edited by the module.
     *
     * @param array $flatSetup
     * @param array|null $default
     * @return array
     */
    public function parseComments($flatSetup, $default = null): array
    {
        $default = $default ?? $flatSetup;
        $categoryLabels = [];
        $editableComments = [];
        $counter = 0;
        foreach ($flatSetup as $const => $value) {
            $key = $const . '..';
            if (substr($const, -2) === '..' || !isset($flatSetup[$key])) {
                continue;
            }
            $counter++;
            $comment = trim($flatSetup[$key]);
            foreach (explode(LF, $comment) as $k => $v) {
                $line = trim(preg_replace('/^[#\\/]*/', '', $v) ?? '');
                if (!$line) {
                    continue;
                }
                foreach (explode(';', $line) as $par) {
                    if (str_contains($par, '=')) {
                        $keyValPair = explode('=', $par, 2);
                        switch (strtolower(trim($keyValPair[0]))) {
                            case 'type':
                                // Type:
                                $editableComments[$const]['type'] = trim($keyValPair[1]);
                                break;
                            case 'cat':
                                // List of categories.
                                $catSplit = explode('/', strtolower($keyValPair[1]));
                                $catSplit[0] = trim($catSplit[0]);
                                if (isset($categoryLabels[$catSplit[0]])) {
                                    $catSplit[0] = $categoryLabels[$catSplit[0]];
                                }
                                $editableComments[$const]['cat'] = $catSplit[0];
                                // This is the subcategory. Must be a key in $this->subCategories[].
                                // catSplit[2] represents the search-order within the subcat.
                                $catSplit[1] = !empty($catSplit[1]) ? trim($catSplit[1]) : '';
                                if ($catSplit[1] && isset($this->subCategories[$catSplit[1]])) {
                                    $editableComments[$const]['subcat_name'] = $catSplit[1];
                                    $orderIdentifier = isset($catSplit[2]) ? trim($catSplit[2]) : $counter;
                                    $editableComments[$const]['subcat'] = ($this->subCategories[$catSplit[1]][1] ?? '')
                                                                          . '/' . $catSplit[1] . '/' . $orderIdentifier . 'z';
                                } elseif (isset($catSplit[2])) {
                                    $editableComments[$const]['subcat'] = 'x/' . trim($catSplit[2]) . 'z';
                                } else {
                                    $editableComments[$const]['subcat'] = 'x/' . $counter . 'z';
                                }
                                break;
                            case 'label':
                                // Label
                                $editableComments[$const]['label'] = trim($keyValPair[1]);
                                break;
                            case 'customcategory':
                                // Custom category label
                                $customCategory = explode('=', $keyValPair[1], 2);
                                if (trim($customCategory[0])) {
                                    $categoryKey = strtolower($customCategory[0]);
                                    $categoryLabels[$categoryKey] = $this->getLanguageService()->sL($customCategory[1]);
                                }
                                break;
                            case 'customsubcategory':
                                // Custom subCategory label
                                $customSubcategory = explode('=', $keyValPair[1], 2);
                                if (trim($customSubcategory[0])) {
                                    $subCategoryKey = strtolower($customSubcategory[0]);
                                    $this->subCategories[$subCategoryKey][0] = $this->getLanguageService()->sL($customSubcategory[1]);
                                }
                                break;
                            // @note added - sorting of subcategories
                            case 'subcategorysorting':
                                // sorting
                                $tabSplit = explode('/', strtolower($keyValPair[1]));
                                $this->subCategorySorting = array_merge((array)$this->subCategorySorting, $tabSplit);
                                break;
                            // @note added
                            case 'inline':
                            case 'showeditrowonly':
                                $editableComments[$const][strtolower(trim($keyValPair[0]))] = trim($keyValPair[1]);
                                break;

                        }
                    }
                }
            }
            if (isset($editableComments[$const])) {
                $editableComments[$const]['name'] = $const;
                $editableComments[$const]['value'] = trim($value);
                $editableComments[$const]['default_value'] = trim((string)($default[$const] ?? ''));
                // If type was not provided, initialize with default value "string".
                $editableComments[$const]['type'] ??= 'string';
            }
        }
        return $editableComments;
    }

    /**
     * @note added
     * @return array
     */
    public function getSubCategorySorting(): array
    {
        return $this->subCategorySorting;
    }


}
