<?php

namespace DKM\ConstantsEditor\TypoScript;

use DKM\ConstantsEditor\Helpers\MiscHelper;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\TypoScript\Parser\ConstantConfigurationParser;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TemplateServiceFactory
{

    /**
     * @return __anonymous|ExtendedTemplateServiceBase|void
     * @throws Exception
     */
    public function getTemplateService()
    {
        $version = (new Typo3Version())->getMajorVersion();
        if($version === 11) return $this->getTemplateServiceV11();
        if($version === 10) return $this->getTemplateServiceV10();
        MiscHelper::throwVersionException();
    }

    /**
     * @return ExtendedTemplateServiceBase|__anonymous
     */
    public function getTemplateServiceV11()
    {
        return new class extends ExtendedTemplateServiceBase {
            /**
             * @param Context|null $context
             * @param \TYPO3\CMS\Core\TypoScript\Parser\ConstantConfigurationParser $constantParser
             */
            public function __construct(Context $context = null, ConstantConfigurationParser $constantParser = null)
            {
                parent::__construct($context);
                $this->constantParser = $constantParser ?? GeneralUtility::makeInstance(ConstantConfigurationParser::class);
                // Disabled in backend context
                $this->tt_track = false;
                $this->verbose = false;
            }

            /**
             * Overridden to:
             * - add inline param
             * - sort subcategories
             * - process header type
             * - use constantsAccessArray to hide fields
             *
             * This functions returns the HTML-code that creates the editor-layout of the module.
             *
             * @param array $theConstants
             * @param string $category
             * @return array
             */
            public function ext_printFields($theConstants, $category): array
            {
                reset($theConstants);
                $groupedOutput = [];
                $subcat = '';

// @note added
                $subCategorySorting = array_flip($this->constantParser->getSubCategorySorting());
                $groupedOutputIndex = 0;

                if (!empty($this->categories[$category]) && is_array($this->categories[$category])) {
                    asort($this->categories[$category]);
                    /** @var IconFactory $iconFactory */
                    $iconFactory = GeneralUtility::makeInstance(IconFactory::class);

// @note changed
                    $categoryLoop = 1000;

                    foreach ($this->categories[$category] as $name => $type) {
                        $params = $theConstants[$name];

// @note added
                        //All constants are default enabled. But access to constants can be enabled and disabled. This enables a branch of constants, while all other will be disabled: wec_config.recursive.[constantsKey] = 1
                        if(!$this->constantAllowed($name)) continue;

                        if (is_array($params)) {
// @note uncommented
//                    if ($subcat !== (string)($params['subcat_name'] ?? '')) {
//                        $categoryLoop++;
//                        $subcat = (string)($params['subcat_name'] ?? '');
//                        $subcat_name = $subcat ? (string)($this->constantParser->getSubCategories()[$subcat][0] ?? '') : 'Others';
//                        $groupedOutput[$categoryLoop] = [
//                            'label' => $subcat_name,
//                            'fields' => [],
//                        ];
//                    }

// @mote inserted
                            if ($subcat !== (string)($params['subcat_name'] ?? '')) {
                                $subcat = (string)($params['subcat_name'] ?? '');
                                $groupedOutputIndex = $subCategorySorting[$subcat] ?? $categoryLoop++;
                                $subcat_name = $subcat ? (string)($this->constantParser->getSubCategories()[$subcat][0] ?: $subcat) : 'Others';
                                $groupedOutput[$groupedOutputIndex] = [
                                    'label' => $subcat_name,
                                    'fields' => [],
                                ];
                            }
                            $label = $this->getLanguageService()->sL($params['label']);
                            $label_parts = explode(':', $label, 2);
                            $head = trim($label_parts[0]);
                            if (count($label_parts) === 2) {
                                $body = trim($label_parts[1]);
                            } else {
                                $body = '';
                            }
                            $typeDat = $this->ext_getTypeData($params['type']);
                            $p_field = '';
                            $fragmentName = substr(md5($params['name']), 0, 10);
                            $fragmentNameEscaped = htmlspecialchars($fragmentName);
                            [$fN, $fV, $params, $idName] = $this->ext_fNandV($params);
                            $idName = htmlspecialchars($idName);
                            $hint = '';
                            switch ($typeDat['type']) {
                                case 'int':
                                case 'int+':
                                    $additionalAttributes = '';
                                    if ($typeDat['paramstr'] ?? false) {
                                        $hint = ' Range: ' . $typeDat['paramstr'];
                                    } elseif ($typeDat['type'] === 'int+') {
                                        $hint = ' Range: 0 - ';
                                        $typeDat['min'] = 0;
                                    } else {
                                        $hint = ' (Integer)';
                                    }

                                    if (isset($typeDat['min'])) {
                                        $additionalAttributes .= ' min="' . (int)$typeDat['min'] . '" ';
                                    }
                                    if (isset($typeDat['max'])) {
                                        $additionalAttributes .= ' max="' . (int)$typeDat['max'] . '" ';
                                    }

                                    $p_field =
                                        '<input class="form-control" id="' . $idName . '" type="number"'
                                        . ' name="' . $fN . '" value="' . $fV . '" data-form-update-fragment="' . $fragmentNameEscaped . '" ' . $additionalAttributes . ' />';
                                    break;
                                case 'color':
                                    $p_field = '
                                <input class="form-control formengine-colorpickerelement t3js-color-picker" type="text" id="input-' . $idName . '" rel="' . $idName .
                                        '" name="' . $fN . '" value="' . $fV . '" data-form-update-fragment="' . $fragmentNameEscaped . '"/>';

                                    $this->javaScriptInstructions['color'] ??= JavaScriptModuleInstruction::forRequireJS('TYPO3/CMS/Backend/ColorPicker')
                                        ->invoke('initialize');
                                    break;
                                case 'wrap':
                                    $wArr = explode('|', $fV);
                                    $p_field = '<div class="input-group">
                                            <input class="form-control form-control-adapt" type="text" id="' . $idName . '" name="' . $fN . '" value="' . $wArr[0] . '" data-form-update-fragment="' . $fragmentNameEscaped . '" />
                                            <span class="input-group-addon input-group-icon">|</span>
                                            <input class="form-control form-control-adapt" type="text" name="W' . $fN . '" value="' . $wArr[1] . '" data-form-update-fragment="' . $fragmentNameEscaped . '" />
                                         </div>';
                                    break;
                                case 'offset':
                                    $wArr = explode(',', $fV);
                                    $labels = GeneralUtility::trimExplode(',', $typeDat['paramstr']);
                                    $p_field = '<span class="input-group-addon input-group-icon">' . ($labels[0] ?: 'x') . '</span><input type="text" class="form-control form-control-adapt" name="' . $fN . '" value="' . $wArr[0] . '" data-form-update-fragment="' . $fragmentNameEscaped . '" />';
                                    $p_field .= '<span class="input-group-addon input-group-icon">' . ($labels[1] ?: 'y') . '</span><input type="text" name="W' . $fN . '" value="' . $wArr[1] . '" class="form-control form-control-adapt" data-form-update-fragment="' . $fragmentNameEscaped . '" />';
                                    $labelsCount = count($labels);
                                    for ($aa = 2; $aa < $labelsCount; $aa++) {
                                        if ($labels[$aa]) {
                                            $p_field .= '<span class="input-group-addon input-group-icon">' . $labels[$aa] . '</span><input type="text" name="W' . $aa . $fN . '" value="' . $wArr[$aa] . '" class="form-control form-control-adapt" data-form-update-fragment="' . $fragmentNameEscaped . '" />';
                                        } else {
                                            $p_field .= '<input type="hidden" name="W' . $aa . $fN . '" value="' . $wArr[$aa] . '" />';
                                        }
                                    }
                                    $p_field = '<div class="input-group">' . $p_field . '</div>';
                                    break;
                                case 'options':
                                    if (is_array($typeDat['params'])) {
                                        $p_field = '';
                                        foreach ($typeDat['params'] as $val) {
                                            $vParts = explode('=', $val, 2);
                                            $label = $vParts[0];
                                            $val = $vParts[1] ?? $vParts[0];
                                            if($params['default_value'] === $val) {
                                                $defaultValueLabel = $label;
                                            }
                                            // option tag:
                                            $sel = '';
                                            if ($val === $params['value']) {
                                                $sel = ' selected';
                                            }
                                            $p_field .= '<option value="' . htmlspecialchars($val) . '"' . $sel . '>' . $this->getLanguageService()->sL($label) . '</option>';
                                        }
                                        $params['default_value'] = $defaultValueLabel ?? ($params['default_value'] ?? '');
                                        $p_field = '<select class="form-select" id="' . $idName . '" name="' . $fN . '" data-form-update-fragment="' . $fragmentNameEscaped . '">' . $p_field . '</select>';
                                    }
                                    break;
                                case 'boolean':
                                    $sel = $fV ? 'checked' : '';
                                    $boolName = $params['default_value'] ? 'yes' : 'no';
                                    $params['default_value'] = $this->getLanguageService()->sL("LLL:EXT:core/Resources/Private/Language/locallang_common.xlf:{$boolName}");
                                    $p_field =
                                        '<input type="hidden" name="' . $fN . '" value="0" />'
                                        . '<label class="btn btn-default btn-checkbox">'
                                        . '<input id="' . $idName . '" type="checkbox" name="' . $fN . '" value="' . (($typeDat['paramstr'] ?? false) ?: 1) . '" ' . $sel . ' data-form-update-fragment="' . $fragmentNameEscaped . '" />'
                                        . '<span class="t3-icon fa"></span>'
                                        . '</label>';
                                    break;
                                case 'comment':
                                    $sel = $fV ? '' : 'checked';
                                    $p_field =
                                        '<input type="hidden" name="' . $fN . '" value="" />'
                                        . '<label class="btn btn-default btn-checkbox">'
                                        . '<input id="' . $idName . '" type="checkbox" name="' . $fN . '" value="1" ' . $sel . ' data-form-update-fragment="' . $fragmentNameEscaped . '" />'
                                        . '<span class="t3-icon fa"></span>'
                                        . '</label>';
                                    break;
                                case 'file':
                                    // extensionlist
                                    $extList = $typeDat['paramstr'];
                                    if ($extList === 'IMAGE_EXT') {
                                        $extList = $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'];
                                    }
                                    $p_field = '<option value="">(' . $extList . ')</option>';
                                    if (trim($params['value'])) {
                                        $val = $params['value'];
                                        $p_field .= '<option value=""></option>';
                                        $p_field .= '<option value="' . htmlspecialchars($val) . '" selected>' . $val . '</option>';
                                    }
                                    $p_field = '<select class="form-select" id="' . $idName . '" name="' . $fN . '" data-form-update-fragment="' . $fragmentNameEscaped . '">' . $p_field . '</select>';
                                    break;
                                case 'user':
                                    $userFunction = $typeDat['paramstr'];
                                    $userFunctionParams = ['fieldName' => $fN, 'fieldValue' => $fV];
                                    $p_field = GeneralUtility::callUserFunction($userFunction, $userFunctionParams, $this);
                                    break;

// @note inserted
                                case 'header':
                                    $p_field = '';
                                    break;

// @note uncommented
//                        default:
//                            $p_field = '<input class="form-control" id="' . $idName . '" type="text" name="' . $fN . '" value="' . $fV . '" data-form-update-fragment="' . $fragmentNameEscaped . '" />';

// @note inserted
                                default:
                                    $hookFunctionFound = false;
                                    // Hook for adding content after Constants Editor
                                    if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['constantseditor']['additionalFieldTypes'])) {
                                        foreach($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['constantseditor']['additionalFieldTypes'] as $object) {
                                            $object = GeneralUtility::makeInstance($object);
                                            $type = $typeDat['type'];
                                            if(is_object($object) && method_exists($object, $type)) {
                                                $hookFunctionFound = true;
                                                $userFunctionParams = array(
                                                    'fieldName' => $fN,
                                                    'fieldValue' => $fV,
                                                    'idName' => $idName,
                                                    'typeDat' => $typeDat,
                                                    'params' => $params,
                                                );
                                                $p_field = $object->{$type}($userFunctionParams, $this);
                                                $params = $userFunctionParams['params'];
                                            }
                                        }
                                    }
                                    if(!$hookFunctionFound) {
                                        $p_field = '<input class="form-control" id="' . $idName . '" type="text" name="' . $fN . '" value="' . $fV . '"'
                                            . 'data-form-update-fragment="' . $fragmentNameEscaped . '" />';
                                    }
                            }
                            // Define default names and IDs
                            $userTyposcriptID = 'userTS-' . $idName;
                            $defaultTyposcriptID = 'defaultTS-' . $idName;
                            $userTyposcriptStyle = '';
                            // Set the default styling options
                            if (isset($this->objReg[$params['name']])) {
                                $checkboxValue = 'checked';
                                $defaultTyposcriptStyle = 'style="display:none;"';
                            } else {
                                $checkboxValue = '';
                                $userTyposcriptStyle = 'style="display:none;"';
                                $defaultTyposcriptStyle = '';
                            }
                            $deleteIconHTML =
                                '<button type="button" class="btn btn-default t3js-toggle" data-bs-toggle="undo" rel="' . $idName . '">'
                                . '<span title="' . htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.deleteTitle')) . '">'
                                . $iconFactory->getIcon('actions-edit-undo', Icon::SIZE_SMALL)->render()
                                . '</span>'
                                . '</button>';
                            $editIconHTML =
                                '<button type="button" class="btn btn-default t3js-toggle" data-bs-toggle="edit"  rel="' . $idName . '">'
                                . '<span title="' . htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.editTitle')) . '">'
                                . $iconFactory->getIcon('actions-open', Icon::SIZE_SMALL)->render()
                                . '</span>'
                                . '</button>';
                            $constantCheckbox = '<input type="hidden" name="check[' . $params['name'] . ']" id="check-' . $idName . '" value="' . $checkboxValue . '"/>';
                            if($typeDat['type'] == 'bool') {

                            }
                            // If there's no default value for the field, use a static label.
                            if ($params['default_value'] == '') {
                                $params['default_value'] = $this->getLanguageService()->sL('LLL:EXT:form/Resources/Private/Language/Database.xlf:empty');
                            }
                            $constantDefaultRow =
                                '<div class="input-group defaultTS" id="' . $defaultTyposcriptID . '" ' . $defaultTyposcriptStyle . '>'
                                . '<span class="input-group-btn">' . $editIconHTML . '</span>'
                                . '<input class="form-control" type="text" placeholder="' . htmlspecialchars($params['default_value']) . '" readonly>'
                                . '</div>';
                            $constantEditRow =
                                '<div class="input-group userTS" id="' . $userTyposcriptID . '" ' . $userTyposcriptStyle . '>'
                                . '<span class="input-group-btn">' . $deleteIconHTML . '</span>'
                                . $p_field
                                . '</div>';

                            // @note added
                            if($params['showeditrowonly'] ?? false) {
                                $constantEditRow = $p_field;
                                $constantCheckbox = '';
                                $constantDefaultRow = '';
                            }

                            $constantData =
                                $constantCheckbox
                                . $constantEditRow
                                . $constantDefaultRow;

                            // @note changed - replace key with $groupedOutputIndex
                            $groupedOutput[$groupedOutputIndex]['items'][] = [
                                'identifier' => $fragmentName,
                                'label' => $head,
                                'name' => $params['name'],
                                'description' => $body,
                                'hint' => $hint,
                                'data' => $constantData,
                                // @note added
                                'inline' => ($params['inline'] ?? ''),
                                // @note added
                                'type' => $typeDat['type']
                            ];
                        } else {
                            debug('Error. Constant did not exist. Should not happen.');
                        }
                    }
                }
                // @note added
                ksort($groupedOutput);
                return $groupedOutput;
            }
        };
    }


    /**
     * @return ExtendedTemplateServiceBase|__anonymous@21270
     */
    public function getTemplateServiceV10()
    {
        return new class extends ExtendedTemplateServiceBase
        {

            /**
             * @param Context|null $context
             * @param \TYPO3\CMS\Core\TypoScript\Parser\ConstantConfigurationParser $constantParser
             */
            public function __construct(Context $context = null, ConstantConfigurationParser $constantParser = null)
            {
//                parent::__construct($context);
//                $this->constantParser = $constantParser ?? GeneralUtility::makeInstance(ConstantConfigurationParser::class);
//                // Disabled in backend context
//                $this->tt_track = false;
//                $this->verbose = false;
            }

            /**
             * This functions returns the HTML-code that creates the editor-layout of the module.
             *
             * @param array $theConstants
             * @param string $category
             * @return string
             */
            public function ext_printFields($theConstants, $category)
            {
//                reset($theConstants);
//                $output = '';
//                $subcat = '';
//
//// @note added
//                $subCategorySorting = array_flip($this->constantParser->getSubCategorySorting());
//                $groupedOutputIndex = 0;
//
//                if (is_array($this->categories[$category])) {
//                    if (!$this->doNotSortCategoriesBeforeMakingForm) {
//                        asort($this->categories[$category]);
//                    }
//                    /** @var IconFactory $iconFactory */
//                    $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
//
//// @note inserted
//                    $categoryLoop = 1000;
//
//                    foreach ($this->categories[$category] as $name => $type) {
//                        $params = $theConstants[$name];
//
//// @note added
//                        //All constants are default enabled. But access to constants can be enabled and disabled. This enables a branch of constants, while all other will be disabled: wec_config.recursive.[constantsKey] = 1
//                        if(!$this->constantAllowed($name)) continue;
//
//                        if (is_array($params)) {
//// @note uncommented
////                    if ($subcat != $params['subcat_name']) {
////                        $subcat = $params['subcat_name'];
////                        $subcat_name = $params['subcat_name'] ? $this->constantParser->getSubCategories()[$params['subcat_name']][0] : 'Others';
////                        $output .= '<h3>' . $subcat_name . '</h3>';
////                    }
//
//// @note inserted
//                            if ($subcat !== (string)($params['subcat_name'] ?? '')) {
//                                $subcat = (string)($params['subcat_name'] ?? '');
//                                $groupedOutputIndex = $subCategorySorting[$subcat] ?? $categoryLoop++;
//                                $subcat_name = $subcat ? (string)($this->constantParser->getSubCategories()[$subcat][0] ?: $subcat) : 'Others';
//                                $groupedOutput[$groupedOutputIndex] = [
//                                    'label' => $subcat_name,
//                                    'fields' => [],
//                                ];
//                            }
//
//
//
//
//                            $label = $this->getLanguageService()->sL($params['label']);
//                            $label_parts = explode(':', $label, 2);
//                            if (count($label_parts) === 2) {
//                                $head = trim($label_parts[0]);
//                                $body = trim($label_parts[1]);
//                            } else {
//                                $head = trim($label_parts[0]);
//                                $body = '';
//                            }
//                            $typeDat = $this->ext_getTypeData($params['type']);
//                            $p_field = '';
//                            $raname = substr(md5($params['name']), 0, 10);
//                            $aname = '\'' . $raname . '\'';
//                            [$fN, $fV, $params, $idName] = $this->ext_fNandV($params);
//                            $idName = htmlspecialchars($idName);
//                            $hint = '';
//                            switch ($typeDat['type']) {
//                                case 'int':
//                                case 'int+':
//                                    $additionalAttributes = '';
//                                    if ($typeDat['paramstr']) {
//                                        $hint = ' Range: ' . $typeDat['paramstr'];
//                                    } elseif ($typeDat['type'] === 'int+') {
//                                        $hint = ' Range: 0 - ';
//                                        $typeDat['min'] = 0;
//                                    } else {
//                                        $hint = ' (Integer)';
//                                    }
//
//                                    if (isset($typeDat['min'])) {
//                                        $additionalAttributes .= ' min="' . (int)$typeDat['min'] . '" ';
//                                    }
//                                    if (isset($typeDat['max'])) {
//                                        $additionalAttributes .= ' max="' . (int)$typeDat['max'] . '" ';
//                                    }
//
//                                    $p_field =
//                                        '<input class="form-control" id="' . $idName . '" type="number"'
//                                        . ' name="' . $fN . '" value="' . $fV . '" onChange="uFormUrl(' . $aname . ')"' . $additionalAttributes . ' />';
//                                    break;
//                                case 'color':
//                                    $p_field = '
//                                <input class="form-control formengine-colorpickerelement t3js-color-picker" type="text" id="input-' . $idName . '" rel="' . $idName .
//                                        '" name="' . $fN . '" value="' . $fV . '" onChange="uFormUrl(' . $aname . ')" />';
//
//                                    if (empty($this->inlineJavaScript[$typeDat['type']])) {
//                                        $this->inlineJavaScript[$typeDat['type']] = 'require([\'TYPO3/CMS/Backend/ColorPicker\'], function(ColorPicker){ColorPicker.initialize()});';
//                                    }
//                                    break;
//                                case 'wrap':
//                                    $wArr = explode('|', $fV);
//                                    $p_field = '<div class="input-group">
//                                            <input class="form-control form-control-adapt" type="text" id="' . $idName . '" name="' . $fN . '" value="' . $wArr[0] . '" onChange="uFormUrl(' . $aname . ')" />
//                                            <span class="input-group-addon input-group-icon">|</span>
//                                            <input class="form-control form-control-adapt" type="text" name="W' . $fN . '" value="' . $wArr[1] . '" onChange="uFormUrl(' . $aname . ')" />
//                                         </div>';
//                                    break;
//                                case 'offset':
//                                    $wArr = explode(',', $fV);
//                                    $labels = GeneralUtility::trimExplode(',', $typeDat['paramstr']);
//                                    $p_field = '<span class="input-group-addon input-group-icon">' . ($labels[0] ?: 'x') . '</span><input type="text" class="form-control form-control-adapt" name="' . $fN . '" value="' . $wArr[0] . '" onChange="uFormUrl(' . $aname . ')" />';
//                                    $p_field .= '<span class="input-group-addon input-group-icon">' . ($labels[1] ?: 'y') . '</span><input type="text" name="W' . $fN . '" value="' . $wArr[1] . '" class="form-control form-control-adapt" onChange="uFormUrl(' . $aname . ')" />';
//                                    $labelsCount = count($labels);
//                                    for ($aa = 2; $aa < $labelsCount; $aa++) {
//                                        if ($labels[$aa]) {
//                                            $p_field .= '<span class="input-group-addon input-group-icon">' . $labels[$aa] . '</span><input type="text" name="W' . $aa . $fN . '" value="' . $wArr[$aa] . '" class="form-control form-control-adapt" onChange="uFormUrl(' . $aname . ')" />';
//                                        } else {
//                                            $p_field .= '<input type="hidden" name="W' . $aa . $fN . '" value="' . $wArr[$aa] . '" />';
//                                        }
//                                    }
//                                    $p_field = '<div class="input-group">' . $p_field . '</div>';
//                                    break;
//                                case 'options':
//                                    if (is_array($typeDat['params'])) {
//                                        $p_field = '';
//                                        foreach ($typeDat['params'] as $val) {
//                                            $vParts = explode('=', $val, 2);
//                                            $label = $vParts[0];
//                                            $val = $vParts[1] ?? $vParts[0];
//                                            // option tag:
//                                            $sel = '';
//                                            if ($val === $params['value']) {
//                                                $sel = ' selected';
//                                            }
//                                            $p_field .= '<option value="' . htmlspecialchars($val) . '"' . $sel . '>' . $this->getLanguageService()->sL($label) . '</option>';
//                                        }
//                                        $p_field = '<select class="form-control" id="' . $idName . '" name="' . $fN . '" onChange="uFormUrl(' . $aname . ')">' . $p_field . '</select>';
//                                    }
//                                    break;
//                                case 'boolean':
//                                    $sel = $fV ? 'checked' : '';
//                                    $p_field =
//                                        '<input type="hidden" name="' . $fN . '" value="0" />'
//                                        . '<label class="btn btn-default btn-checkbox">'
//                                        . '<input id="' . $idName . '" type="checkbox" name="' . $fN . '" value="' . ($typeDat['paramstr'] ?: 1) . '" ' . $sel . ' onClick="uFormUrl(' . $aname . ')" />'
//                                        . '<span class="t3-icon fa"></span>'
//                                        . '</label>';
//                                    break;
//                                case 'comment':
//                                    $sel = $fV ? '' : 'checked';
//                                    $p_field =
//                                        '<input type="hidden" name="' . $fN . '" value="" />'
//                                        . '<label class="btn btn-default btn-checkbox">'
//                                        . '<input id="' . $idName . '" type="checkbox" name="' . $fN . '" value="1" ' . $sel . ' onClick="uFormUrl(' . $aname . ')" />'
//                                        . '<span class="t3-icon fa"></span>'
//                                        . '</label>';
//                                    break;
//                                case 'file':
//                                    // extensionlist
//                                    $extList = $typeDat['paramstr'];
//                                    if ($extList === 'IMAGE_EXT') {
//                                        $extList = $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'];
//                                    }
//                                    $p_field = '<option value="">(' . $extList . ')</option>';
//                                    if (trim($params['value'])) {
//                                        $val = $params['value'];
//                                        $p_field .= '<option value=""></option>';
//                                        $p_field .= '<option value="' . htmlspecialchars($val) . '" selected>' . $val . '</option>';
//                                    }
//                                    $p_field = '<select class="form-select" id="' . $idName . '" name="' . $fN . '" onChange="uFormUrl(' . $aname . ')">' . $p_field . '</select>';
//                                    break;
//                                case 'user':
//                                    $userFunction = $typeDat['paramstr'];
//                                    $userFunctionParams = ['fieldName' => $fN, 'fieldValue' => $fV];
//                                    $p_field = GeneralUtility::callUserFunction($userFunction, $userFunctionParams, $this);
//                                    break;
//
//// @note insterted
//                                case 'header':
//                                    $p_field = '';
//                                    break;
//
//// @note uncommented
////                        default:
////                            $p_field = '<input class="form-control" id="' . $idName . '" type="text" name="' . $fN . '" value="' . $fV . '"'
////                                . ' onChange="uFormUrl(' . $aname . ')" />';
//
//// @note inserted
//                                default:
//                                    $hookFunctionFound = false;
//                                    // Hook for adding content after Constants Editor
//                                    if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['constantseditor']['additionalFieldTypes'])) {
//                                        foreach($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['constantseditor']['additionalFieldTypes'] as $object) {
//                                            $object = GeneralUtility::makeInstance($object);
//                                            $type = $typeDat['type'];
//                                            if(is_object($object) && method_exists($object, $type)) {
//                                                $hookFunctionFound = true;
//                                                $userFunctionParams = array(
//                                                    'fieldName' => $fN,
//                                                    'fieldValue' => $fV,
//                                                    'idName' => $idName,
//                                                    'typeDat' => $typeDat,
//                                                    'params' => $params,
//                                                );
//                                                $p_field = $object->{$type}($userFunctionParams, $this);
//                                            }
//                                        }
//                                    }
//                                    if(!$hookFunctionFound) {
//                                        $p_field = '<input class="form-control" id="' . $idName . '" type="text" name="' . $fN . '" value="' . $fV . '"'
//                                            . '" onChange="uFormUrl(' . $aname . ')" />';
//                                    }
//                            }
//                            // Define default names and IDs
//                            $userTyposcriptID = 'userTS-' . $idName;
//                            $defaultTyposcriptID = 'defaultTS-' . $idName;
//                            $checkboxName = 'check[' . $params['name'] . ']';
//                            $checkboxID = 'check-' . $idName;
//                            $userTyposcriptStyle = '';
//                            $deleteIconHTML = '';
//                            $constantCheckbox = '';
//                            $constantDefaultRow = '';
//                            if (!$this->ext_dontCheckIssetValues) {
//                                // Set the default styling options
//                                if (isset($this->objReg[$params['name']])) {
//                                    $checkboxValue = 'checked';
//                                    $defaultTyposcriptStyle = 'style="display:none;"';
//                                } else {
//                                    $checkboxValue = '';
//                                    $userTyposcriptStyle = 'style="display:none;"';
//                                    $defaultTyposcriptStyle = '';
//                                }
//                                $deleteTitle = htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.deleteTitle'));
//                                $deleteIcon = $iconFactory->getIcon('actions-edit-undo', Icon::SIZE_SMALL)->render();
//                                $deleteIconHTML =
//                                    '<button type="button" class="btn btn-default t3js-toggle" data-toggle="undo" rel="' . $idName . '">'
//                                    . '<span title="' . $deleteTitle . '">'
//                                    . $deleteIcon
//                                    . '</span>'
//                                    . '</button>';
//                                $editTitle = htmlspecialchars($this->getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.editTitle'));
//                                $editIcon = $iconFactory->getIcon('actions-open', Icon::SIZE_SMALL)->render();
//                                $editIconHTML =
//                                    '<button type="button" class="btn btn-default t3js-toggle" data-toggle="edit"  rel="' . $idName . '">'
//                                    . '<span title="' . $editTitle . '">'
//                                    . $editIcon
//                                    . '</span>'
//                                    . '</button>';
//                                $constantCheckbox = '<input type="hidden" name="' . $checkboxName . '" id="' . $checkboxID . '" value="' . $checkboxValue . '"/>';
//                                // If there's no default value for the field, use a static label.
//                                if ($params['default_value'] == '') {
//                                    $params['default_value'] = '[Empty]';
//                                }
//                                $constantDefaultRow =
//                                    '<div class="input-group defaultTS" id="' . $defaultTyposcriptID . '" ' . $defaultTyposcriptStyle . '>'
//                                    . '<span class="input-group-btn">' . $editIconHTML . '</span>'
//                                    . '<input class="form-control" type="text" placeholder="' . htmlspecialchars($params['default_value']) . '" readonly>'
//                                    . '</div>';
//                            }
//                            $constantEditRow =
//                                '<div class="input-group userTS" id="' . $userTyposcriptID . '" ' . $userTyposcriptStyle . '>'
//                                . '<span class="input-group-btn">' . $deleteIconHTML . '</span>'
//                                . $p_field
//                                . '</div>';
//                            $constantLabel = '<label class="t3js-formengine-label"><span>' . htmlspecialchars($head) . '</span></label>';
//                            $constantName = '<span class="help-block">[' . $params['name'] . ']</span>';
//                            $constantDescription = $body ? '<p class="help-block">' . htmlspecialchars($body) . '</p>' : '';
//                            $constantData = '';
//                            if ($hint !== '') {
//                                $constantData .= '<span class="help-block">' . $hint . '</span>';
//                            }
//                            $constantData .=
//                                $constantCheckbox
//                                . $constantEditRow
//                                . $constantDefaultRow;
//
//// @note uncommented
//                            $output .=
//                                '<fieldset class="form-section">'
//                                . '<a name="' . $raname . '"></a>'
//                                . '<div class="form-group">'
//                                . $constantLabel . $constantName . $constantDescription . $constantData
//                                . '</div>'
//                                . '</fieldset>';
//
//// @note inserted
//                            $groupedOutput[$groupedOutputIndex]['items'][] = [
//                                'identifier' => $raname,
//                                'label' => strip_tags($constantLabel),
//                                'name' => $constantName,
//                                'description' => strip_tags(html_entity_decode($constantDescription)),
////                                'hint' => $hint,
//                                'data' => $constantData,
//                                'inline' => $params['inline'],
//                                'type' => $typeDat['type']
//                            ];
//
//                        } else {
//                            debug('Error. Constant did not exist. Should not happen.');
//                        }
//                    }
//                }
//                ksort($groupedOutput);
//                return array_values($groupedOutput);
            }
        };
    }
}