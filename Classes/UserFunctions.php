<?php
namespace DKM\ConstantsEditor;
/***************************************************************
 * Copyright notice
 *
 * (c) 2007 Foundation For Evangelism (info@evangelize.org)
 * (c) 2009 Georg Ringer <www.ringer.it>
 * All rights reserved
 *
 * This file is part of the Web-Empowered Church (WEC)
 * (http://webempoweredchurch.org) ministry of the Foundation for Evangelism
 * (http://evangelize.org). The WEC is developing TYPO3-based
 * (http://typo3.org) free software for churches around the world. Our desire
 * is to use the Internet to help offer new life through Jesus Christ. Please
 * see http://WebEmpoweredChurch.org/Jesus.
 *
 * You can redistribute this file and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This file is distributed in the hope that it will be useful for ministry,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the file!
 ***************************************************************/

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * TODO: Localize this script
 *
 */
class UserFunctions
{
    function inputModal($params, $pObj)
    {
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
        $conf = $this->getConf($fieldName, $pObj);

        if($extConfPath = $conf['modalUrlExtensionCfgPath'] ?? false) {
            list($extensionKey, $cfgPath) = explode(':', $extConfPath);
            if($modalContentUrl = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get($extensionKey, $cfgPath)) {

                $modalContentUrl .= (parse_url($modalContentUrl, PHP_URL_QUERY) ? '&' : '?') . 'constant=' . $params['params']['name'];

                $input = '<div><input readonly class="modalmodal btn btn-default" type="button" name="' . $fieldName . '" value="' . $conf['buttonLabel'] . '" id="' . $fieldName . '"></div>';

                $js =
                    <<<JAVASCRIPT
                <script type="text/javascript">
        document.getElementById('$fieldName').addEventListener('click', function(){
            require([
              'TYPO3/CMS/Backend/Modal'
            ], function(Modal) {
            const configuration = {
              type: Modal.types.iframe,
              title: '',
              content: '$modalContentUrl',
              size: Modal.sizes.medium,
            };
            Modal.advanced(configuration);
            })
        })
                </script>
        JAVASCRIPT;
            };
        }
        return $input . $js;
    }




    /**
     * Builds an input form with a colorpicker
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Object
     * @return        string        HTML output
     *
     * http://jscolor.com/
     * http://design.geckotribe.com/colorwheel/
     * Maybe this one is much better: http://www.dematte.at/colorPicker/
     *
     **/
    function colorPicker($params, $pObj)
    {
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
        $input = '<input class="jscolor {hash:true,required:false}" size="10" name="' . $fieldName . '" value="' . $fieldValue . '" id="' . $fieldName . '" />';
        return $input;
    }

    /**
     * @param $params
     * @param $pObj
     */
    function baseColorPicker($params, $pObj)
    {
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
    }


    /**
     * Builds an input form with a colorpicker
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Object
     * @return        string        HTML output
     *
     * http://jscolor.com/
     * Alternative: http://www.dematte.at/colorPicker/
     **/
    function colorPickerAdv($params, $pObj)
    {
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
        $input = '<input class="jscolor {hash:true,required:false} constantseditor-colorpickeradv-input" size="10" name="' . $fieldName . '" value="' . $fieldValue . '" id="' . $fieldName . '" />';
        return $input;
    }

    /**
     * @param $params
     * @param $pObj
     * @return string
     */
    function optionsAdvanced(&$params, $pObj)
    {
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
        $optionsConf = $this->getConf($fieldName, $pObj, 'options');
        $settingsConf = $this->getConf($fieldName, $pObj, 'settings');
        $settingsConf['cleanIntValue'] = (int)($settingsConf['cleanIntValue'] ?? 0);

        $val = $fieldValue;
        $label = $fieldValue;
        $p_field = '<option value="' . htmlspecialchars($val) . '">Valgt værdi: ' . $GLOBALS['LANG']->sL($label) . ($settingsConf['cleanIntValue'] ? $settingsConf['unit'] : '') . '</option>';
        if (is_array($optionsConf)) {
            foreach ($optionsConf as $key => $val) {
                $vParts = explode('=', $val, 2);
                $label = $vParts[0];
                $val = $key;
                if($params['params']['default_value'] === $val) {
                    $defaultValueLabel = $label;
                }
                // option tag:
                $sel = '';
                if ($val == $fieldValue) {
                    $sel = ' selected';
                }
                $p_field .= '<option value="' . htmlspecialchars($val) . '"' . $sel . '>' . $GLOBALS['LANG']->sL($label) . '</option>';
            }
            $params['params']['default_value'] = $defaultValueLabel ?? ($params['params']['default_value'] ?? '');
            if(($settingsConf['nooptionalvalue'] ?? 0) !=1 ) {
                $val = 'alternative';
                $label = 'Vælg anden værdi';
                $p_field .= '<option value="' . htmlspecialchars($val) . '">' . $GLOBALS['LANG']->sL($label) . '</option>';
            }
            $p_field = '<select class="constantseditor-optionsadvanced-select" id="' . $fieldName . '" name="' . $fieldName . '">' . $p_field . '</select>';
        }
        $output = '<input type="text"  class="constantseditor-optionsadvanced-input" style="display:none;" size="30" value="" data-unit="' . ($settingsConf['unit'] ?? '') . '" data-noUnitOnValue="' . $settingsConf['cleanIntValue'] . '" id="' . $fieldName . '_input"  onfocus="this.value=\'\'" />';
        return $p_field . $output;
    }

    /**
     * @param $params
     * @param $pObj
     * @return string
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException
     *
     * Inspiration for this function:
     * http://www.sk-typo3.de/Folder-Selektor-im-BE-Modul.367.0.html
     *
     * ###Examples:###
     * Pages:
     * type=elementBrowser;
     *
     * Pages with no manual input field for urls:
     * settings=disableManualInput:1; type=elementBrowser;
     *
     * Dam records of the type gif and jpg:
     * settings=table:sys_file,extensions:gif|jpg; type=elementBrowser;
     *
     * Folders:
     * settings=mode:folder; type=elementBrowser;
     *
     */
    public function elementBrowser(&$params, $pObj): string
    {
        //@todo test the above mentioned settings
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];
        $preview = '';
        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);
        $mode = ($conf['mode'] ?? '') ?: 'db';
        if ($mode == 'db') {
            $table = $conf['table'] ?: 'pages';
            if ($table == 'sys_file') {
                $allowedExtensions = $conf['extensions'] ? str_replace('|', ',', $conf['extensions']) : 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png';
            }
            $fac = GeneralUtility::makeInstance(ResourceFactory::class); // create instance to storage repository
            switch ($table) {
                case 'sys_file':
                    $hideInput = 1;
                    if (is_numeric($fieldValue)) {
                        /** @var ResourceFactory $fac */
                        try {
                            $file = $fac->getFileObject($fieldValue);
                            $fileFound = true;
                        } catch (FileDoesNotExistException $e) {
                            $fileFound = false;
                            $preview = 'Fil med nedenstående id blev ikke fundet.';
                        } catch (\InvalidArgumentException $e) {
                            $fileFound = false;
                            $preview = $e->getMessage();
                        }
                        if ($fileFound) {
                            $title = $file->getName();
                            if (GeneralUtility::inList('gif,jpg,jpeg,tif,tiff,bmp,png', strtolower($file->getExtension()))) {
                                $thumb = $file->process(\TYPO3\CMS\Core\Resource\ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, array(
                                    'width' => '600m',
                                    'height' => 400,
                                    'additionalParameters' => '-quality 50'
                                ))->getPublicUrl();
                                $preview = '<br/><img src="' . $thumb . '" alt="preview"/>';
                            } else {
                                $preview = '';
                            }
                        }
                    }
                    break;
                case 'pages':
                    $select_fields = 'title';
                case 'tt_content':
                    $hideInput = 1;
                    if (is_numeric($fieldValue)) {
                        /** @var PageRepository $pageRepository */
                        $pageRepository = GeneralUtility::makeInstance(PageRepository::class);
                        $pagesRow = $pageRepository->getRawRecord($table, $fieldValue, (isset($select_fields) && $select_fields ? $select_fields : 'header'));
                        $title = $pagesRow[$select_fields] . ' (id:' . $fieldValue . ')';
                    }
                    break;
            }
            if (($table == 'pages' || $table == 'tt_content' || $table == 'sys_file') && !($title ?? '')) {
                $title = $fieldValue ?: '[ingen valgt]';
            }
        } else {
            $hideInput = 0;
        }
        $clearLink = !$fieldValue ? '' : '<a id="' . $fieldName . '_clear"  class="constantseditor-elementbrowser-clear btn btn-default" href="#">' . $iconFactory->getIcon('actions-input-clear', Icon::SIZE_SMALL)->render() . '&nbsp;Fjern</a>';
        $icon = '<a  id="' . $fieldName . '_open" class="constantseditor-elementbrowser-open btn btn-default" href="#" data-type="' . ($table == 'sys_file' ? 'file' : 'db') . '" data-params="' . ($table == 'sys_file' ? $allowedExtensions : $table) . '">' .
            $iconFactory->getIcon('actions-insert-record', Icon::SIZE_SMALL)->render() .
            '&nbsp;Vælg fil</a>';

        if($table==='pages') {
            $icon = '<a  id="' . $fieldName . '_open" class="constantseditor-elementbrowser-open btn btn-default" href="#" data-type="' . ($table == 'sys_file' ? 'file' : 'db') . '" data-params="' . ($table == 'sys_file' ? $allowedExtensions : $table) . '">' .
            $iconFactory->getIcon('apps-pagetree-page-shortcut-external', Icon::SIZE_SMALL)->render() .
            '&nbsp;Vælg side</a>';
        }
        //$preview - code for previewing a dam image
        if ($preview) {
            $preview = '<div class="constantseditor-preview" id="' . $fieldName . '_preview">' . $preview . '</div>';
        }


        //$input_title dummy field for showing value in a more human readable way. For example if table is pages, then the id is translated to the page title.
        if (isset($title)) {
            $output[] = '<input class="form-control" id="' . $fieldName . '_title" type="text" size="30" disabled="disabled" value="' . $title . '">';
        }

        //$input - the input field can be hidden, if the value is gotten from a wizard, because the value is not always user-friendly (for example pages_123)
        $input = '<input class="form-control" type="' . ($hideInput ? 'hidden' : 'text') . '" size="30" name="' . $fieldName . '" value="' . $fieldValue . '" id="' . $fieldName . '" />';
        if ($hideInput) {
            $outputHidden = $input;
        } else {
            $output[] = $input;
        }

        if ($defaultValue = ($params['params']['default_value'] ?? false)) {
            if (is_numeric($defaultValue)) {
                $file = $fac->getFileObject($defaultValue);
                $params['params']['default_value'] = $file->getName();
            }
        }


        return '<span class="input-group-btn">' . $clearLink . $icon . '</span>' . implode('<br/>', $output) . $outputHidden . $preview;
    }


//    /**
//     * Builds a record list of any table
//     * @param        array $params :  Contains fieldName and fieldValue.
//     * @param        obj $pObj :  Objet
//     * @return        string        HTML output
//     */
//    function recordList($params, $pObj)
//    {
//        //@todo1 migrate to TYPO3 CMS 9LTS
//
//        /* Pull the current fieldname and value from constants */
//        $fieldName = $params['fieldName'];
//        $fieldValue = $params['fieldValue'];
//
//        // get the configuration
//        $conf = $this->getConf($fieldName, $pObj);
//
//        $table = $conf['table'];
//        $where = ($conf['where'] != '') ? $conf['where'] : '1=1';
//        $orderBy = $conf['orderBy'];
//        $limit = $conf['limit'];
//
//
//        if ($table == '') {
//            return 'Table "' . $table . '" doesn\'t exit';
//        }
//
//        /* Construct the SQL query */
//        $res = $GLOBALS['TYPO3_DB']->exec_selectQuery('*', $table, $where . t3lib_beFunc::deleteClause($table), '', $orderBy, $limit);
//
//        /* Build the HTML select tag */
//        $content = array();
//        $content[] = '<select name="' . $fieldName . '">';
//        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
//            $label = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordTitle($table, $row);
//
//            /* If the current user matches the field value, mark it as default */
//            if ($row['uid'] == $fieldValue) {
//                $selected = 'selected="selected"';
//            } else {
//                $selected = '';
//            }
//
//            /* Build the option tag */
//            $content[] = '<option value="' . $row['uid'] . '" ' . $selected . '>' . $label . '</option>';
//        }
//        $content[] = '</select>';
//
//        return implode(chr(10), $content);
//    }

    /**
     * Show an iframe
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Objet
     * @return        string        HTML output
     */
    function iframe($params, $pObj)
    {
        //@todo1 test and migrate

        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];


        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);
        $settings = '';

        $conf['src'] = ($conf['https'] == 1) ? 'https://' . $conf['src'] : 'http://' . $conf['src'];
        unset ($conf['https']);


        foreach ($conf as $key => $value) {
            $settings .= $key . '="' . $value . '" ';
        }

        $iframe = '<iframe ' . $settings . ' ></iframe>';

        return $iframe;
    }


    /**
     * Show an image, mainly for helping people (manual, ...)
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Objet
     * @return        string        HTML output
     */
    function html($params, $pObj)
    {
        //@todo1 test and migrate

        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];


        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);

        $search = array('#58#', '#59#', '#44#');
        $replace = array(':', ';', ',');
        $code = str_replace($search, $replace, $conf['code']);

        return $code;
    }


    /**
     * Show an image, mainly for helping people (manual, ...)
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Objet
     * @return        string        HTML output
     */
    function header($params, $pObj)
    {
        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];


        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);

        $search = array('#58#', '#59#', '#44#');
        $replace = array(':', ';', ',');
        $code = str_replace($search, $replace, $conf['code']);

        return '<h2 style="margin-bottom:-20px;border-bottom: 1px solid;">' . $conf['code'] . '</h2>';
    }

    /**
     * Builds an textarea
     * @param        array $params :  Contains fieldName and fieldValue.
     * @param        obj $pObj :  Objet
     * @return        string        HTML output
     */
    function textarea($params, $pObj)
    {
        /*two javascript functions added to php addJSfunctions: function changeContent() and window.onload=function()*/

        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];

        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);
        $key = substr($fieldName, 5, -1);

        $functionNameAppend = substr(strrchr($key, '.'), 1);
        $formName = !empty($conf['formName']) ? $conf['formName'] : 'editform';

        $css = '';
        unset($conf['formName']);
        foreach ($conf as $csskey => $cssvalue) {
            $css .= $csskey . ':' . $cssvalue . '; ';
        }

        if ($css != '') {
            $css = ' style="' . $css . '" ';
        }

        $fieldValue = str_replace("&lt;br/&gt;", "\n", $fieldValue);
        $field = '<textarea id="field1' . $key . '"  ' . $css . '>' . $fieldValue . '</textarea>
		<input type="hidden" id="field2' . $key . '" name="' . $fieldName . '" value=""/>';

        // add necessary js modification to change linefeeds

        $js = '
		<script type="text/javascript">
			formFieldArray = ( typeof formFieldArray != \'undefined\' && formFieldArray instanceof Array ) ? formFieldArray : [];
			formFieldArray.push("' . $key . '");
		</script>
		
		';

        $field .= $js;

        return $field;
    }

    /**
     * Get the possible configuration of a single field
     * @param        string $fieldName :  Name of the field
     * @param        obj $pObj :  Objet
     * @return        array        configuration
     */
    private function getConf($fieldName, $pObj, $configName = 'settings')
    {
        $key = substr($fieldName, 5, -1);
        $key = str_replace('.', '/', $key) . '..';
        $tempConf = '';
        $realConf = array();
        // get the complete line from constants and find the key 'settings'
        $conf = GeneralUtility::trimExplode(';', ArrayUtility::getValueByPath(GeneralUtility::removeDotsFromTS($pObj->setup['constants']), $key));
        foreach ($conf as $key => $value) {
            if (strpos($value, $configName) !== false) {
                $tempConf = $value;
            }
        }
        // if settings are found, split them accordingly
        if ($tempConf != '') {
            $tempConf = substr($tempConf, strlen($configName) + 1);

            $tempConf = GeneralUtility::trimExplode(',', $tempConf);

            foreach ($tempConf as $key) {
                $split = GeneralUtility::trimExplode(':', $key, 0, 2);
                $realConf[$split[0]] = ($split[1] ?? '');
            }

        }

        return $realConf;
    }

    /**
     * Builds an input form of the password type.
     * @param		array		$params:  Contains fieldName and fieldValue.
     * @param		obj		$pObj:  Objet
     * @return		string		HTML output
     */
    function password($params, $pObj) {
        /* Pull the current fieldname and value from constants */
        $fieldName = $params['fieldName'];
        $fieldValue = $params['fieldValue'];

        // get the configuration
        $conf = $this->getConf($fieldName, $pObj);
        $formName = !empty($conf['formName']) ? $conf['formName'] : 'editform';

        $input = '<input type="password" name="'. $fieldName .'" value="' . $fieldValue . '" onFocus="this.select()" />';

        return $input;
    }
}
