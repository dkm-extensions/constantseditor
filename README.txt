=====
Constants Editor
=====


Introduction
============

This documentation should be rewritten to conform with: https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ExtensionArchitecture/Documentation/Index.html

TYPO3 Constants Editor for any backend user. Up to 10 user configurable backend modules each with their own set of constants to edit.

Configuration
=============
Important parts regarding configuration:

extension settings
TypoScript Constant Category: constants.fixedinfo

pageTS
mod.constantseditor.recursive.constants = 1

Constants
constants.fixedinfo {
#tabs=1

# cat=constants.fixedinfo/diverse/b5; type=string; label=Google Analytics: Her indtaster du det UA-nummer, som du har fået udleveret fra Google Analytics (f.eks. UA-1843712-4).
googleanalytics =
}