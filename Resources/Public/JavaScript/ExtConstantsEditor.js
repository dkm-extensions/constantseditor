TYPO3.settings.FormEngine = {"formName":"editform"};

define([
    'jquery',
    'TYPO3/CMS/Backend/Modal',
    'TYPO3/CMS/Backend/FormEngine',
    'TYPO3/CMS/Backend/Utility/MessageUtility'
], function ($, Modal, FormEngine, MessageUtility) {
    'use strict';
    var ExtConstantsEditor = {
        options: {
            optionsAdvancedSelector: '.constantseditor-optionsadvanced-select',
            optionsAdvancedInput: '.constantseditor-optionsadvanced-input',
            colorPickerAdvInput: '.constantseditor-colorpickeradv-input',
            elementBrowserClear: '.constantseditor-elementbrowser-clear',
            elementBrowserOpen: '.constantseditor-elementbrowser-open'
        }
    };
    // ***************
    // Used to connect the db/file browser with this document and the formfields on it!
    // ***************
    ExtConstantsEditor.browserWin="";

    ExtConstantsEditor.setFormValueOpenBrowserOld = function (mode,params) {	//
        var url = "' . $BACK_PATH . 'browser.php?mode="+mode+"&bparams="+params;

        ExtConstantsEditor.browserWin = window.open(url,"Typo3WinBrowser","height=350,width="+(mode=="db"?650:600)+",status=0,menubar=0,resizable=1,scrollbars=1");
        ExtConstantsEditor.browserWin.focus();
    };

    window.addEventListener('message', function (e) {
        if (!MessageUtility.MessageUtility.verifyOrigin(e.origin)) {
            throw 'Denied message sent by ' + e.origin;
        }

        if (typeof e.data.fieldName === 'undefined') {
            throw 'fieldName not defined in message';
        }

        if (typeof e.data.value === 'undefined') {
            throw 'value not defined in message';
        }

        if (e.data.actionName === 'typo3:elementBrowser:elementAdded') {
            var titleField = document.getElementById(e.data.fieldName + '_title');
            if (titleField) {
                titleField.value = e.data.label;
            }
            var previewField = document.getElementById(e.data.fieldName + '_preview');
            if (previewField) {
                previewField.innerHTML = "<h2 style=\"border:solid 3px; padding:10px; margin:10px; display:inline-block;\">Nyt valg foretaget, gem for at se ændringen.</h2>";
            }
        }
    });

    ExtConstantsEditor.setComplementaryColors = function(fName) {
        var color = document.getElementById(fName).value;
        var invertedColor = ExtConstantsEditor.inverse(document.getElementById(fName).value);
        var fNameInverted = fName.slice(0,-1) + "_inverted]";
        if(document.getElementById(fNameInverted)) {
            document.getElementById(fNameInverted).value = invertedColor;
            document.getElementById(fNameInverted).style.backgroundColor = invertedColor;
    //                document.getElementById("colorbox-" + fNameInverted).style.backgroundColor = invertedColor;
        }
    };
    ExtConstantsEditor.inverse = function (theString) {
        var DecToHex = function (number) {
            var hexbase = "0123456789ABCDEF";
            return hexbase.charAt((number >> 4) & 0xf) + hexbase.charAt(number & 0xf);
        };
        var giveHex = function (s) {
            s = s.toUpperCase();
            return parseInt(s, 16);
        };
        var matches = theString.match("(#)*([0123456789ABCDEFabcdef]{6})(.*)");
        if (matches[2] && !matches[3]) {
            var r, g, c, r1, r2, g1, g2, c1, c2, newColor;
            var colorCode = matches[2];
            r = colorCode.slice(0, 2);
            g = colorCode.slice(2, 4);
            c = colorCode.slice(4, 6);
            r1 = 16 * giveHex(r.slice(0, 1));
            r2 = giveHex(r.slice(1, 2));
            r = r1 + r2;
            g1 = 16 * giveHex(g.slice(0, 1));
            g2 = giveHex(g.slice(1, 2));
            g = g1 + g2;
            c1 = 16 * giveHex(c.slice(0, 1));
            c2 = giveHex(c.slice(1, 2));
            c = c1 + c2;
            newColor = DecToHex(255 - r) + "" + DecToHex(255 - g) + "" + DecToHex(255 - c);
            return matches[1] + newColor;
        } else {
            window.alert('You Must Enter a six digit color code');
            return false;
        }
    };

    ExtConstantsEditor.setFormValueClear = function(fName){
        document.getElementById(fName).value = '';
        if(document.getElementById(fName + "_title")) {
            document.getElementById(fName + "_title").value = '';
        }
        if(document.getElementById(fName + "_preview")) {
            document.getElementById(fName + "_preview").innerHTML="<h2 style=\"border:solid 3px; padding:10px; margin:10px; display:inline-block;\">Indhold af feltet slettet, gem for at se ændringen.</h2>";
        }
    };

    ExtConstantsEditor.switchOptions = function(fName) {
        var input = document.getElementById(fName + '_input');
        input.style.display = 'block';
        document.getElementById(fName).style.display = 'none';
    };

    ExtConstantsEditor.addOption = function(fName,unit,cleanInt) {
        var addUnitToLabel, addUnitToValue;
        if (cleanInt) {
            addUnitToLabel = unit;
            addUnitToValue = '';
        } else {
            addUnitToLabel = unit;
            addUnitToValue = unit;
        }
        var select = document.getElementById(fName);
        var manualInput = document.getElementById(fName + '_input');
        var newValue = manualInput.value;
        if(unit) {
            if(parseInt(newValue)) {
                newValue = parseInt(newValue);
            } else {
                newValue = '';
                alert('Fejl i indtastning! Indtast kun tal.');
            }
        }
        select.style.display = 'block';
        manualInput.style.display = 'none';
        select.options[0] = new Option('Valgt værdi: ' + newValue + addUnitToLabel, newValue + addUnitToValue);
        select.options[0].selected = 1;
    };

    /**
     * formFieldArray is defined in Classes/UserFunctions.php
     */
    ExtConstantsEditor.changeContent = function() {
        if(typeof formFieldArray === 'object') {
            for(var fieldNameKey in formFieldArray) {
                if(formFieldArray.hasOwnProperty(fieldNameKey)){
                    var fieldName = formFieldArray[fieldNameKey];
                    var str2 = document.getElementById("field1" + fieldName).value;
                    while(str2.indexOf("\r\n") != -1) {
                        str2 = str2.replace("\r\n", "<br/>");
                    }
                    while(str2.indexOf("\n") != -1) {
                        str2 = str2.replace("\n", "<br/>");
                    }
                    document.getElementById("field2" + fieldName).value = str2
                }
            }
        }
    };
    /*Added for multiline textarea functionality*/

    ExtConstantsEditor.initializeEvents = function() {
        const getBaseName = function (name) {
            return name.substr(0, name.lastIndexOf('_'));
        };
        function initLoad() {
            const el = document.getElementsByName("editform");
            $(el[0]).on("submit", ExtConstantsEditor.changeContent);
            document.getElementsByClassName("color");
        }
        if(document.readyState === 'complete') {
            initLoad();
        } else {
            window.onload = initLoad;
        }

        $(document).on('change', ExtConstantsEditor.options.optionsAdvancedSelector, function() {
            const elementId = $(this).attr('id');
            const el = document.getElementById(elementId);
            if(el.options[el.selectedIndex].value === 'alternative') ExtConstantsEditor.switchOptions(elementId);
        }).on('change', ExtConstantsEditor.options.optionsAdvancedInput, function() {
            ExtConstantsEditor.addOption(getBaseName($(this).attr('id')), $(this).data('unit'), $(this).data('noUnitOnValue'));
        }).on('change', ExtConstantsEditor.options.colorPickerAdvInput, function () {
            ExtConstantsEditor.setComplementaryColors($(this).attr('id'));
        }).on('click', ExtConstantsEditor.options.elementBrowserClear, function () {
            ExtConstantsEditor.setFormValueClear(getBaseName($(this).attr('id')), 'Remove');
        }).on('click', ExtConstantsEditor.options.elementBrowserOpen, function () {
            FormEngine.openPopupWindow($(this).data('type'), getBaseName($(this).attr('id')) + '|||' + $(this).data('params'));
        })
    };

    ExtConstantsEditor.initializeEvents();
    return ExtConstantsEditor;
});

