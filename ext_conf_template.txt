#customsubcategory=module1=LLL:EXT:dkmconfig/locallang.xml:module1
#customsubcategory=module2=LLL:EXT:dkmconfig/locallang.xml:module2
#customsubcategory=module3=LLL:EXT:dkmconfig/locallang.xml:module3
modulemain {
# cat=modulemain/basic/0; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=modulemain/basic/1; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=modulemain/basic/2; type=input; label=Short module title (in module menu)
moduleTab = Styling for Content

# cat=modulemain/basic/3; type=input; label=Module title
moduleTitle = def

# cat=modulemain/basic/4; type=input; label=Description of module
moduleDescription = gi
}
module1 {
# cat=module1/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule = 1

# cat=module1/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory = content.cTextmedia

# cat=module1/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = font

# cat=module1/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module1/basic/5; type=input; label=Short module title (in module menu)
moduleTab = Styling for Content

# cat=module1/basic/6; type=input; label=Module title
moduleTitle = Content styling

# cat=module1/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module1/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS = 

# cat=module1/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module2 {
# cat=module2/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule = 1

# cat=module2/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory = content.templates

# cat=module2/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = edit

# cat=module2/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module2/basic/5; type=input; label=Short module title (in module menu)
moduleTab = Styling for templates

# cat=module2/basic/6; type=input; label=Module title
moduleTitle = adfs

# cat=module2/basic/7; type=input; label=Description of module
moduleDescription = ewrwer

# cat=module2/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module2/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module3 {
# cat=module3/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module3/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module3/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module3/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module3/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module3/basic/6; type=input; label=Module title
moduleTitle =

# cat=module3/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module3/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module3/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module4 {
# cat=module4/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module4/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module4/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module4/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module4/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module4/basic/6; type=input; label=Module title
moduleTitle =

# cat=module4/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module4/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module4/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module5 {
# cat=module5/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module5/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module5/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module5/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module5/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module5/basic/6; type=input; label=Module title
moduleTitle =

# cat=module5/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module5/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module5/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module6 {
# cat=module6/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module6/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module6/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module6/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module6/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module6/basic/6; type=input; label=Module title
moduleTitle =

# cat=module6/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module6/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module6/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module7 {
# cat=module7/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module7/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module7/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module7/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module7/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module7/basic/6; type=input; label=Module title
moduleTitle =

# cat=module7/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module7/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module7/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module8 {
# cat=module8/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module8/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module8/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module8/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module8/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module8/basic/6; type=input; label=Module title
moduleTitle =

# cat=module8/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module8/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module8/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module9 {
# cat=module9/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module9/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module9/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module9/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module9/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module9/basic/6; type=input; label=Module title
moduleTitle =

# cat=module9/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module9/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =
    
# cat=module9/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}

module10 {
# cat=module10/basic/1; type=boolean; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
enableModule =

# cat=module10/basic/2; type=input; label=TypoScript Constant Category: Category to use in the "Edit Site Constants" section. If no value is given, module will not be displayed.
constantCategory =

# cat=module10/basic/3; type=options[Cogs=cogs,Cog=cog,Wrench=wrench,Sliders=sliders,Tasks=tasks,Edit=edit,Paragraph=paragraph,Pen=pen,Pencil=pencil,Pencil2=pencil-alt,Underline=underline,Text=text,Bold=bold,Font=font,Heading=heading]; label=Icon identifier
iconIdentifier = cogs

# cat=module10/basic/4; type=input; label=Font awesome Icon: Alternative Font Awesome icon
iconIdentifierOverride =

# cat=module10/basic/5; type=input; label=Short module title (in module menu)
moduleTab =

# cat=module10/basic/6; type=input; label=Module title
moduleTitle =

# cat=module10/basic/7; type=input; label=Description of module
moduleDescription =

# cat=module10/basic/8; type=string; label=Pid from UserTS path: The userTS path to find the pid from, if not root page
pidFromUserTS =

# cat=module10/basic/9; type=string; label=MainModuleKey: if not constantsEditor
mainModule =
}