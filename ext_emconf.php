<?php

########################################################################
# Extension Manager/Repository config file for ext "dkmintranet".
#
# Auto generated 21-09-2012 09:45
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Constants Editor',
	'description' => 'TYPO3 Constants Editor for any backend user. Up to 10 user configurable backend modules each with their own set of constants to edit.',
	'category' => '',
	'author' => 'Stig Færch',
	'author_email' => 'snf(at)dkm.dk',
	'shy' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.2.11',
	'constraints' => array(
		'depends' => array(
			'tstemplate' => ''
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => '',
	'suggests' => array(
	),
);
