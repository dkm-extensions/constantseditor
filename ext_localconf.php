<?php
if (TYPO3_MODE === 'BE') {
    $config = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('constantseditor');
    $icons = [];
    foreach ($config as $moduleKey => $moduleConfig) {
        if($moduleKey == 'modulemain' || ($moduleConfig['enableModule'] ?? false)) {
            $icons['ext_constanteditor-' . $moduleKey] = $moduleConfig['iconIdentifierOverride'] ? $moduleConfig['iconIdentifierOverride'] : $moduleConfig['iconIdentifier'];
        }
    }
    /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    foreach ($icons as $identifier => $name) {
        $iconRegistry->registerIcon(
            $identifier,
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => $name]
        );
    }
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['constantseditor']['additionalFieldTypes'][] = DKM\ConstantsEditor\UserFunctions::class;



    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateConstantEditorModuleFunctionController::class] = [
        'className' => \DKM\ConstantsEditor\Controller\TypoScriptTemplateConstantEditorModuleFunctionController::class
    ];

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\TypoScript\Parser\ConstantConfigurationParser::class] = [
        'className' => \DKM\ConstantsEditor\TypoScript\Parser\ConstantConfigurationParser::class
    ];

}