<?php

defined('TYPO3') or die();

call_user_func(function () {
    $addModule = function ($moduleKey, $moduleConfig) {
        $constantsMainModule = ($moduleConfig['mainModule'] ?? false) ?: 'constantsEditor';
        $name = $constantsMainModule . ($moduleKey ? "_$moduleKey" : '');
        $config = [
            'name' => $name,
            'icon' => '',
            'iconIdentifier' => 'ext_constanteditor-' . ($moduleKey ?: 'modulemain'),
            'labels' => [
                'labels' => [
                    'tablabel' => $moduleConfig['moduleTitle'],
                    'tabdescr' => $moduleConfig['moduleDescription'],
                ],
                'tabs' => [
                    'tab' => $moduleConfig['moduleTab']
                ]
            ],
        ];
        if ($moduleKey !== '') {
            $config['routeTarget'] = \DKM\ConstantsEditor\Controller\ConstantsEditorModuleController::class . "::$name";
            $config['access'] = 'user,group';
            $config['constantsEditor'] = ['constantCategory' => $moduleConfig['constantCategory'], 'moduleTitle' => $moduleConfig['moduleTitle'], 'pidFromUserTS' => $moduleConfig['pidFromUserTS'] ?? null, 'mainModule' => $moduleConfig['mainModule'] ?? null];
        }
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule(
            $constantsMainModule,
            $moduleKey,
            'bottom',
            '',
            $config
        );
    };

    try {
        $config = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('constantseditor');

        $addModule('', $config['modulemain']);
        unset($config['modulemain']);
        ksort($config, SORT_NATURAL);
        foreach ($config as $moduleKey => $module) {
            if(!str_starts_with($moduleKey, 'module')) continue;
            if ($module['enableModule'] && count(array_filter(array_intersect_key($module, ['constantCategory' => 1, 'moduleTab' => 1]))) == 2) {
                $addModule($moduleKey, $module);
            }
        }
    } catch (\TYPO3\CMS\Core\Exception $exception) {
        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['constantseditor']['configurationError'] = $exception->getMessage();
        $addModule('1', 'ext_dkmshortcut-calendar');
    }
    $GLOBALS['TBE_STYLES']['skins']['backend']['stylesheetDirectories']['constantEditorModuleIconStyling'] = 'EXT:constantseditor/Resources/Public/Css/Backend/';
});
